/**
 * MATH
 */

// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.
// What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)

let smallPizza = 13;
let largePizza = 17;

let smallPizzaArea = Math.PI * (smallPizza / 2) ** 2;
let largePizzaArea = Math.PI * (largePizza / 2) ** 2;

// 2. What is the cost per square inch of each pizza?

let costOf13 = 16.99/smallPizzaArea;
let costOf17 = 19.99/largePizzaArea;

// 3. Using the Math object, put together a code snippet
// that allows you to draw a random card with a value
// between 1 and 13 (assume ace is 1, jack is 11…)

function myCard() {
    return Math.floor(Math.random() * Math.floor(13) + 1);
}

// 4. Draw 3 cards and use Math to determine the highest
// card

let myCard1 = myCard();
let myCard2 = myCard();
let myCard3 = myCard();

let maxCard = Math.max(myCard1, myCard2, myCard3);

/**
 * ADDRESS LINE
 */

// 1. Create variables for firstName, lastName,
// streetAddress, city, state, and zipCode. Use
// this information to create a formatted address block
// that could be printed onto an envelope.

const firstName = 'Sean';
const lastName = 'Chao';
const streetAddress = '1234 Main St.';
const city = 'Seattle';
const state = 'WA';
const zip = '98001';

// 2. You are given a string in this format:
// firstName lastName(assume no spaces in either)
// streetAddress
// city, state zip(could be spaces in city and state)
// 
// Write code that is able to extract the first name from this string into a variable.
// Hint: use indexOf, slice, and / or substring

let mailingAddress = `${firstName} ${lastName}
${streetAddress}
${city}, ${state} ${zip}`;

let myFirstName = mailingAddress.substring(0, 4);

/**
 * FIND THE MIDDLE DATE
 */
// On your own find the middle date(and time) between the following two dates:
// 1/1/2020 00:00:00 and 4/1/2020 00:00:00
//
// Look online for documentation on Date objects.

// Starting hint:

const startDate = new Date('2020-01-01T00:00:00');
const endDate = new Date('2020-04-01T00:00:00');
let middleDate = new Date((startDate.getTime() + endDate.getTime()) / 2);
